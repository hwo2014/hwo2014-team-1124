package phonon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import phonon.basicbot.message.get.CarParser;
import phonon.basicbot.message.get.CarPositionMessage;
import phonon.basicbot.message.get.CarPositionParser;
import phonon.basicbot.message.get.CrashMessage;
import phonon.basicbot.message.get.GameInitMessage;
import phonon.basicbot.message.get.GetMessage;
import phonon.basicbot.message.get.Piece;
import phonon.basicbot.message.get.YourCarMessage;
import phonon.basicbot.message.send.Join;
import phonon.basicbot.message.send.SendMsg;
import phonon.basicbot.message.send.Switch;
import phonon.basicbot.message.send.Throttle;

import com.google.gson.Gson;

public class Main {
	public static void main(String args[]) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		new Main(reader, writer, new Join(botName, botKey));
	}

	final Gson myGson = new Gson();
	private PrintWriter myWriter;
	private Model myModel;
	private String myCarName;

	public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
		this.myWriter = writer;
		Logger logger = LoggerFactory.getLogger(getClass().getName());
		String line = null;
		send(join);

		while ((line = reader.readLine()) != null) {
			final GetMessage msgFromServer = myGson.fromJson(line, GetMessage.class);
			if (msgFromServer.getMsgType().equals("gameInit")) {
				raceInit(line);
				logger.info("Race init track:" + myModel.getTrackName());
			}
			if (msgFromServer.getMsgType().equals("gameStart")) {
				logger.info("Race start");
			}
			if (msgFromServer.getMsgType().equals("yourCar")) {
				YourCarMessage msg = myGson.fromJson(line, YourCarMessage.class);
				myCarName = msg.getData().getName();
				logger.info("my car " + myCarName);
			}
			if (msgFromServer.getMsgType().equals("crash")) {
				CrashMessage msg = myGson.fromJson(line, CrashMessage.class);
				if (msg.getData().getName().equals(myCarName)) {
					Car car = myModel.getCar(myCarName);
					String res = "";
					res += " tick: " + myModel.getTick();
					res += " piece: " + car.getInPieceDistance();
					int pieceIndex = car.getPieceIndex();
					res += " ind: " + pieceIndex;
					res += " car angle: " + car.getAngle();
					res += " speed: " + myModel.getCarSpeed(myCarName);

					Piece piece = myModel.getPiece(pieceIndex);
					res += " radius: "
							+ (piece.getRadius() - Math.signum(piece.getAngle())
									* myModel.getLanes(car.getEndLaneIndex()));
					res += " piece angle: " + piece.getAngle();
					logger.info(res);
				}

			}
			if (msgFromServer.getMsgType().equals("carPositions")) {
				
				updateModel(line);
				Car car = myModel.getCar(myCarName);
				if (car.getPieceIndex()==1){
					send(Switch.Right());
				}
				if (car.getPieceIndex()==6){
					send(Switch.Left());
				}
				if (car.getPieceIndex()==20){
					send(Switch.Right());
				}
				int tick = myModel.getTick();
				double throttle = 0.65;
				// if ((tick / 200) % 2 == 0)
				// throttle = 0.8;

				if (logger.isDebugEnabled()) {
					String res = "";
					res += " tick: " + myModel.getTick();
					res += " piece: " + car.getInPieceDistance();
					int pieceIndex = car.getPieceIndex();
					res += " ind: " + pieceIndex;
					res += " angle: " + car.getAngle();
					res += " speed: " + myModel.getCarSpeed(myCarName);
					res += " throttle: " + throttle;
					Piece piece = myModel.getPiece(pieceIndex);
					if (piece.isBend()) {
						res += " radius: " + piece.getRadius();
						res += " piece angle: " + piece.getAngle();
					} else {
						res += " length: " + piece.getLength();
					}

					logger.debug(res);
				}
				send(new Throttle(throttle));
			}
			if (msgFromServer.getMsgType().equals("gameEnd")) {
				logger.info("Race ended");
			}
		}

	}

	private void updateModel(String line) {
		CarPositionMessage msg = myGson.fromJson(line, CarPositionMessage.class);
		myModel.setTick(msg.getGameTick());
		CarPositionParser[] positions = msg.getData();
		int n = positions.length;
		for (int i = 0; i < n; i++) {
			CarPositionParser pos = positions[i];
			String name = pos.getId().getName();
			double angle = pos.getAngle();
			double dist = pos.getPiecePosition().getInPieceDistance();
			int lap = pos.getPiecePosition().getLap();
			int pieceIndex = pos.getPiecePosition().getPieceIndex();
			int start = pos.getPiecePosition().getLane().getStartLaneIndex();
			int end = pos.getPiecePosition().getLane().getEndLaneIndex();
			Car car = myModel.getCar(name);
			car.setAngle(angle);
			car.setLap(lap);
			car.setPieceIndex(pieceIndex);
			car.setInPieceDistance(dist);
			car.setStartLaneIndex(start);
			car.setEndLaneIndex(end);

		}

	}

	private void raceInit(String line) {
		GameInitMessage msg = myGson.fromJson(line, GameInitMessage.class);
		int n = msg.getData().getRace().getTrack().getLanes().length;
		double[] lanes = new double[n];
		for (int i = 0; i < n; i++) {
			lanes[msg.getData().getRace().getTrack().getLanes()[i].getIndex()] = msg.getData().getRace().getTrack()
					.getLanes()[i].getDistanceFromCenter();
		}
		Piece[] pieces = msg.getData().getRace().getTrack().getPieces();
		String name = msg.getData().getRace().getTrack().getName();
		Model model = new Model(pieces, lanes, name);
		CarParser[] cars = msg.getData().getRace().getCars();
		n = cars.length;
		for (int i = 0; i < n; i++) {
			CarParser carParser = cars[i];
			Car car = new Car(carParser.getId().getName(), carParser.getDimensions().getLength(), carParser
					.getDimensions().getWidth(), carParser.getDimensions().getGuideFlagPosition());
			model.setCar(cars[i].getId().getName(), car);
		}
		model.setLaps(msg.getData().getRace().getRaceSession().getLaps());
		model.setQuickRace(msg.getData().getRace().getRaceSession().isQuickRace());
		model.setMaxLapTimeMs(msg.getData().getRace().getRaceSession().getMaxLapTimeMs());
		myModel = model;

	}

	private void send(final SendMsg msg) {
		myWriter.println(msg.toJson());
		myWriter.flush();
	}

}
