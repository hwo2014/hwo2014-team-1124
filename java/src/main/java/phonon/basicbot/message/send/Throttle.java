package phonon.basicbot.message.send;

import phonon.basicbot.message.send.SendMsg;

public class Throttle extends SendMsg {
	private double myValue;

	public Throttle(double value) {
		this.myValue = value;
	}

	@Override
	public Object msgData() {
		return myValue;
	}

	@Override
	public String msgType() {
		return "throttle";
	}
}
