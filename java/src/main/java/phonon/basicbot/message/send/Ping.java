package phonon.basicbot.message.send;

import phonon.basicbot.message.send.SendMsg;

public class Ping extends SendMsg {

	@Override
	public String msgType() {
		return "ping";
	}

	@Override
	public String toJson() {
		return "{\"msgType\":\"ping\"}";
	}

}
