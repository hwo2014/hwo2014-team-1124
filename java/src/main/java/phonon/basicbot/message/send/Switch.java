package phonon.basicbot.message.send;

import phonon.basicbot.message.send.SendMsg;

public class Switch extends SendMsg {
	public String myDirection;

	public static Switch Left() {
		Switch switch1 = new Switch();
		switch1.myDirection = "Left";
		return switch1;
	}

	public static Switch Right() {
		Switch switch1 = new Switch();
		switch1.myDirection = "Right";
		return switch1;
	}

	private Switch() {
	}

	@Override
	public String msgType() {
		return "switchLane";
	}

	@Override
	public Object msgData() {
		return myDirection;
	}
}
