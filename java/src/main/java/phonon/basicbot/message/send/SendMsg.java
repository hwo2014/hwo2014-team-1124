package phonon.basicbot.message.send;

import com.google.gson.Gson;

public abstract class SendMsg {

	private static Gson myGson = new Gson();

	private class SendMsgWrapper {
		public final String msgType;
		public final Object data;

		public SendMsgWrapper(final String msgType, final Object data) {
			this.msgType = msgType;
			this.data = data;
		}

		public SendMsgWrapper(final SendMsg sendMsg) {
			this(sendMsg.msgType(), sendMsg.msgData());
		}

	}

	public Object msgData() {
		return this;
	}

	public String toJson() {
		return myGson.toJson(new SendMsgWrapper(this));
	}

	public abstract String msgType();

}
