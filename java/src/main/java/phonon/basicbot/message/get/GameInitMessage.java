package phonon.basicbot.message.get;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class GameInitMessage extends GetMessage {
	public class GameInitData {
		private RaceParser race;

		public RaceParser getRace() {
			return race;
		}

	}

	private GameInitData data;

	public GameInitData getData() {
		return data;
	}

}
