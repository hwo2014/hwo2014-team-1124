package phonon.basicbot.message.get;

public class CarPositionMessage extends GetMessage{
	private int gameTick;
	private String gameId;
	private CarPositionParser[] data;
	
	public int getGameTick() {
		return gameTick;
	}
	
	public CarPositionParser[] getData() {
		return data;
	}
	
	public String getGameId() {
		return gameId;
	}

}
