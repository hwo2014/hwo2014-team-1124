package phonon.basicbot.message.get;

public class CarPositionParser {

	public class PiecePositionParser {

		public class LanePositionParser {
			private int startLaneIndex;
			private int endLaneIndex;

			public int getEndLaneIndex() {
				return endLaneIndex;
			}

			public int getStartLaneIndex() {
				return startLaneIndex;
			}
		}

		private int pieceIndex;
		private double inPieceDistance;
		private int lap;
		private LanePositionParser lane;

		public double getInPieceDistance() {
			return inPieceDistance;
		}

		public int getLap() {
			return lap;
		}

		public LanePositionParser getLane() {
			return lane;
		}

		public int getPieceIndex() {
			return pieceIndex;
		}

	}

	private CarIdParser id;
	private double angle;
	private PiecePositionParser piecePosition;

	public CarIdParser getId() {
		return id;
	}

	public double getAngle() {
		return angle;
	}

	public PiecePositionParser getPiecePosition() {
		return piecePosition;
	}

}
