package phonon.basicbot.message.get;

import com.google.gson.annotations.SerializedName;

public class Piece {

	private double length;
	@SerializedName("switch")
	private boolean mySwitch = false;
	private double radius = 0.0;
	private double angle = 0.0;

	public Piece() {
	}

	public double getLength() {
		return length;
	}

	public boolean isSwitch() {
		return mySwitch;
	}

	public boolean isBend() {
		return !(radius == 0.0);
	}

	public double getAngle() {
		return angle;
	}

	public double getRadius() {
		return radius;
	}

}
