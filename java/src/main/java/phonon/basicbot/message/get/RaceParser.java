package phonon.basicbot.message.get;

public class RaceParser {
	private TrackParser track;
	private CarParser[] cars;
	private RaceSession raceSession;

	public class RaceSession {
		private int laps, maxLapTimeMs;
		private boolean quickRace;

		public boolean isQuickRace() {
			return quickRace;
		}

		public int getLaps() {
			return laps;
		}

		public int getMaxLapTimeMs() {
			return maxLapTimeMs;
		}
	}

	public RaceSession getRaceSession() {
		return raceSession;
	}

	public CarParser[] getCars() {
		return cars;
	}

	public TrackParser getTrack() {
		return track;
	}

}
