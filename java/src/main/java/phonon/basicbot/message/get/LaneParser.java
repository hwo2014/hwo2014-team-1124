package phonon.basicbot.message.get;

public class LaneParser {

	private double distanceFromCenter;
	private int index;

	public double getDistanceFromCenter() {
		return distanceFromCenter;
	}

	public int getIndex() {
		return index;
	}
}
