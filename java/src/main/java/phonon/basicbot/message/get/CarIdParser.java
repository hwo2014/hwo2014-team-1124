package phonon.basicbot.message.get;

public class CarIdParser {
	private String name;
	private String color;

	public String getColor() {
		return color;
	}

	public String getName() {
		return name;
	}
}
