package phonon.basicbot.message.get;

public class TrackParser {
	private String id;
	private String name;
	private Piece[] pieces;
	private LaneParser[] lanes;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Piece[] getPieces() {
		return pieces;
	}

	public LaneParser[] getLanes() {
		return lanes;
	}
}
