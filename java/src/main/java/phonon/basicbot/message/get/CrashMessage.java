package phonon.basicbot.message.get;

public class CrashMessage extends GetMessage {
	private CarIdParser data;
	private int gameTick;
	private String gameId;

	public String getGameId() {
		return gameId;
	}

	public int getGameTick() {
		return gameTick;
	}

	public CarIdParser getData() {
		return data;
	}

}
