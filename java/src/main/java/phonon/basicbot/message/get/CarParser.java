package phonon.basicbot.message.get;

public class CarParser {
		public class CarDimension {
		private double length, width, guideFlagPosition;

		public double getLength() {
			return length;
		}

		public double getWidth() {
			return width;
		}

		public double getGuideFlagPosition() {
			return guideFlagPosition;
		}
	}

	private CarIdParser id;
	private CarDimension dimensions;

	public CarIdParser getId() {
		return id;
	}

	public CarDimension getDimensions() {
		return dimensions;
	}

}
