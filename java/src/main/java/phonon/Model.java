package phonon;

import java.util.ArrayList;
import java.util.HashMap;

import phonon.basicbot.message.get.Piece;

public class Model {
	private Piece[] myPieces;
	String myName;
	private double[] myLanes;
	private int myTick;
	private int myLaps, myMaxLapTimeMs;
	private boolean isQuickRace;
	private HashMap<String, Car> myCars = new HashMap<String, Car>();
	private ArrayList<String> myCarNames = new ArrayList<String>();

	public Model(Piece[] pieces, double[] lanes, String name) {
		myPieces = pieces;
		myLanes = lanes;
		myName = name;
	}

	public String getTrackName() {
		return myName;
	}

	public int getTrackLength() {
		return myPieces.length;
	}

	public Piece getPiece(int index) {
		return myPieces[index];
	}

	public int getLanesNumber() {
		return myLanes.length;
	}

	public double getLanes(int index) {
		return myLanes[index];
	}

	public Car getCar(String name) {
		return myCars.get(name);
	}

	public void setCar(String name, Car car) {
		myCars.put(name, car);
		myCarNames.add(name);
	}

	public void removeCar(String name) {
		myCars.remove(name);
		myCarNames.remove(name);
	}

	public String[] getCarNames() {
		String[] names = new String[myCarNames.size()];
		names = myCarNames.toArray(names);
		return names;
	}

	public void setTick(int gameTick) {
		myTick = gameTick;
	}

	public int getTick() {
		return myTick;
	}

	public int getLaps() {
		return myLaps;
	}

	public void setLaps(int laps) {
		this.myLaps = laps;
	}

	public int getMaxLapTimeMs() {
		return myMaxLapTimeMs;
	}

	public void setMaxLapTimeMs(int maxLapTimeMs) {
		this.myMaxLapTimeMs = maxLapTimeMs;
	}

	public boolean isQuickRace() {
		return isQuickRace;
	}

	public void setQuickRace(boolean quickRace) {
		this.isQuickRace = quickRace;
	}

	public double getCarSpeed(String name) {
		Car car = getCar(name);
		return car.getSpeed(myPieces, myLanes);
	}

	public double getCarPrevSpeed(String name) {
		Car car = getCar(name);
		return car.getPrevSpeed(myPieces, myLanes);
	}

}
