package phonon;

import phonon.basicbot.message.get.Piece;

public class Car {
	// static
	private String myName;
	private double myLength, myWidth, myGuideFlagPosition;
	// dynamic
	private int pieceIndex, prevIndex, prevPrevIndex;
	private double inPieceDistance, prevInPieceDistance, prevPrevInPieceDistance;
	private int lap;
	private double angle, prevAngle;
	private int startLaneIndex;
	private int endLaneIndex;

	public Car(String name, double length, double width, double guideFlagPosition) {
		myName = name;
		myLength = length;
		myWidth = width;
		myGuideFlagPosition = guideFlagPosition;
	}

	public String getName() {
		return myName;
	}

	public void setName(String name) {
		this.myName = name;
	}

	public double getLength() {
		return myLength;
	}

	public void setLength(double length) {
		this.myLength = length;
	}

	public double getWidth() {
		return myWidth;
	}

	public void setWidth(double width) {
		this.myWidth = width;
	}

	public double getGuideFlagPosition() {
		return myGuideFlagPosition;
	}

	public void setGuideFlagPosition(double guideFlagPosition) {
		this.myGuideFlagPosition = guideFlagPosition;
	}

	public int getPieceIndex() {
		return pieceIndex;
	}

	public void setPieceIndex(int pieceIndex) {
		this.prevPrevIndex = this.prevIndex;
		this.prevIndex = this.pieceIndex;
		this.pieceIndex = pieceIndex;
	}

	public double getInPieceDistance() {
		return inPieceDistance;
	}

	public void setInPieceDistance(double inPieceDistance) {
		this.prevPrevInPieceDistance = this.prevInPieceDistance;
		this.prevInPieceDistance = this.inPieceDistance;
		this.inPieceDistance = inPieceDistance;
	}

	private double getSpeed(int i1, int i2, double x1, double x2, Piece[] pieces, double[] lanes) {
		if (i1 == i2)
			return x1 - x2;
		Piece p = pieces[i2];
		double l = 0;
		if (p.isBend()) {
			double r = p.getRadius() - Math.signum(p.getAngle()) * lanes[endLaneIndex];
			l = Math.abs(p.getAngle()) / 360 * 2 * Math.PI * (r);
		} else {
			l = p.getLength();
		}
		return x1 - x2 + l;

	}

	public double getSpeed(Piece[] pieces, double[] lanes) {
		return getSpeed(pieceIndex, prevIndex, inPieceDistance, prevInPieceDistance, pieces, lanes);
	}

	public double getPrevSpeed(Piece[] pieces, double[] lanes) {
		return getSpeed(prevIndex, prevPrevIndex, prevInPieceDistance, prevPrevInPieceDistance, pieces, lanes);
	}

	public int getLap() {
		return lap;
	}

	public void setLap(int lap) {
		this.lap = lap;
	}

	public double getAngle() {
		return angle;
	}

	public double getPrevAngle() {
		return prevAngle;
	}

	public void setAngle(double angle) {
		this.prevAngle = this.angle;
		this.angle = angle;
	}

	public int getStartLaneIndex() {
		return startLaneIndex;
	}

	public void setStartLaneIndex(int startLaneIndex) {
		this.startLaneIndex = startLaneIndex;
	}

	public int getEndLaneIndex() {
		return endLaneIndex;
	}

	public void setEndLaneIndex(int endLaneIndex) {
		this.endLaneIndex = endLaneIndex;
	}

}
