package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.net.Socket;

import com.google.gson.Gson;

public class Main {
	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		new Main(reader, writer, botName, botKey);
	}

	final Gson gson = new Gson();
	private PrintWriter writer;

	public Main(final BufferedReader reader, final PrintWriter writer, final String botName, final String botKey)
			throws IOException {
		this.writer = writer;
		String line = null;

		send(new Join(botName, botKey));

		while ((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
			if (msgFromServer.msgType.equals("carPositions")) {

				final MsgClassPosition msg = gson.fromJson(line, MsgClassPosition.class);
				if (msg.gameTick % 10 == 0 || msg.gameTick % 10 == 1 || msg.gameTick % 10 == 2)
					send(new Throttle(0.6));
				else
					send(new Throttle(0.0));
			} else if (msgFromServer.msgType.equals("join")) {
				System.out.println("Joined");
			} else if (msgFromServer.msgType.equals("gameInit")) {
				System.out.println("Race init");
			} else if (msgFromServer.msgType.equals("gameEnd")) {
				System.out.println("Race end");
			} else if (msgFromServer.msgType.equals("gameStart")) {
				System.out.println("Race start");
			} else {
				send(new Ping());
			}
		}
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}
}

abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}

class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}
}

class MsgClassPosition {
	public final String msgType;
	public final Object data;
	public final int gameTick;

	MsgClassPosition(final String msgType, final Object data, final int gameTick) {
		this.msgType = msgType;
		this.data = data;
		this.gameTick = gameTick;
	}
}

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}