__author__ = 'dima'
from bot import Bot


class FBot(Bot):
    def __init__(self, socket, name, key):
        super(FBot, self).__init__(socket, name, key)

    def calculate_throttle(self):
        if self.tick < 6:
            return 1.0
        else:
            return 0.0
