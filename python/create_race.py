import sys
import bot
import race
import super_bot
import bot

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        race.race(sys.argv, "createRace", super_bot.SBot)