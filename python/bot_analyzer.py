from bot import *
import math


class ABot(Bot):
    def __init__(self, socket, name, key):
        self.tick = 0
        self.mass = 0
        self.gamma = None
        self.inv_gamma = 0
        self.c = 1
        self.v0 = 0
        super(ABot, self).__init__(socket, name, key)

    def calculate_params(self):
        # calculate mass and gamma
        v0, v1, v2 = self.cars.cars[self.name].hist_speed[-3:]
        a1 = v2 - v1
        a0 = v1 - v0
        gamma_div_m = math.log(a1 / a0)
        factor = a1 / a0
        self.gamma = (factor - 1) / (factor * v0 - v1)
        self.inv_gamma = 1 / self.gamma
        self.mass = -self.gamma / gamma_div_m
        self.c = (v1 - v0) / (math.exp(-2 * self.gamma / self.mass) - math.exp(-self.gamma / self.mass))
        self.v0 = v0

