import sys
import bot
import bot_analyzer
import super_bot
import race
import bbot
import superoverbot

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        #race.race(sys.argv, "join", super_bot.SBot)
        race.race(sys.argv, "join", superoverbot.SuperOverBot)
