__author__ = 'dima'

from super_bot import SBot
from bot import logger
import random
import math


class SuperOverBot(SBot):
    def __init__(self, socket, name, key):
        super(SuperOverBot, self).__init__(socket, name, key)
        self.regime = "default"  # default, overtake, bump_out
        self.bump_car = None

    def need_change_lane(self):
        lane = super(SuperOverBot, self).need_change_lane()
        my_lane = self.get_my_car().cur_pos.endLane
        if lane != my_lane:
            return lane
        car = self.is_car_infront()
        lane_num = len(self.track.lanes)
        if car and self.get_my_car().cur_speed*self.gamma < 0.7:
            if not self.meet_before_turn(car, self.throttle_factor):
                ind = (self.get_my_car().cur_pos.pieceIndex + 1) % len(self.track.pieces)
                next_piece = self.track.pieces[ind]
                if not next_piece.switch:
                    return self.desired_lane
                logger.warning("regime="+self.regime)
                if my_lane > 0:
                    other_lane = my_lane - 1
                else:
                    if my_lane == lane_num - 1:
                        other_lane = my_lane - 1
                    else:
                        other_lane = my_lane + 1
                return other_lane
        return my_lane

    def on_turbo_available(self, data, **kwargs):
        super(SuperOverBot, self).on_turbo_available(data, **kwargs)
        logger.warn("turbo available")
        if len(self.piece_length) == 0:
            return
        self.straight_max = (0, 0)
        n = len(self.track.pieces)
        ind = self.absIndex
        while ind < min(self.absIndex + n, len(self.piece_length)):
            max_straight = 0
            j = 0
            logger.debug("ind=" + str(ind) + " j=" + str(j))
            while self.track.pieces[(j + ind) % n].is_straight():
                j += 1
                max_straight += self.piece_length[(j + ind) % n][0]
                if j + ind >= len(self.piece_length) - 1:
                    break
            if self.straight_max[1] < max_straight:
                self.straight_max = (ind % n, max_straight)
            ind += 1
        logger.warn(" straight max=" + str(self.straight_max))


    def need_turbo(self):
        my_car = self.get_my_car()
        ind = my_car.cur_pos.pieceIndex
        piece = self.track.pieces[ind]
        if piece.is_round():
            return False
        another_car = self.is_car_infront()
        if another_car:
            my_ind = self.get_my_car().cur_pos.pieceIndex
            other_ind = another_car.cur_pos.pieceIndex
            all_straight = all([self.track.is_straight(i) for i in xrange(my_ind, other_ind + 1)])
            if self.meet_before_turn(another_car, self.turboFactor) and all_straight:
                logger.info("Turbo to kick tick {0}".format(self.tick))
                return True
        else:
            #logger.warning("someth")
            ind, mlen = self.straight_max
            if self.cars.cars[self.name].cur_pos.pieceIndex == ind and mlen > 0:
                self.straight_max = (0, 0)
                logger.info("Turbo to speed tick {0}".format(self.tick))
                return True
        return False

    def is_car_infront(self):
        my_car = self.get_my_car()
        for name, car in self.cars.cars.iteritems():
            if name == self.name:
                continue

            if my_car.cur_pos.endLane == car.cur_pos.endLane:
                if 0 < (car.real_dist - my_car.real_dist) < self.mass * self.inv_gamma ** 2 * 3 / 5:
                    return car

        return None

    def calculate_throttle(self):
        another_car = self.is_car_infront()
        self.regime = "default"
        if self.tick <= 6:
            return super(SuperOverBot, self).calculate_throttle()

        if another_car:
            my_ind = self.get_my_car().cur_pos.pieceIndex
            other_ind = another_car.cur_pos.pieceIndex
            all_straight = all([self.track.is_straight(i) for i in xrange(my_ind, other_ind + 1)])
            logger.info("another car "+str(all_straight))
            if self.meet_before_turn(another_car, self.throttle_factor) and all_straight:
                logger.warning("bump_out")
                self.regime = "bump_out"
                self.bump_car = another_car

        if self.regime == "default":
            logger.debug("default")
            return super(SuperOverBot, self).calculate_throttle()
        elif self.regime == "bump_out":
            if not self.meet_before_turn(self.bump_car, self.throttle_factor):
                self.regime = "default"
                return 0.0
            return 1.0

    def on_crash(self, data, **kwargs):
        super(SuperOverBot, self).on_crash(data, **kwargs)
        logger.warn("crashed in regime {0}".format(self.regime))

    def distance(self, car):
        my_car = self.get_my_car()

    def meet_before_turn(self, car, factor):
        dist = 0
        my_car = self.get_my_car()
        #time_to_meet = (car.real_dist - my_car.real_dist) / (my_car.cur_speed - car.cur_speed)
        next_round = self.track.next_round_piece(my_car.cur_pos.pieceIndex)
        if car.cur_speed < 0.000001:
            return False
        if (self.track.real_dist[next_round - 1] - car.real_dist) > self.mass * self.inv_gamma ** 2:
            return False
        time_to_turn = (self.track.real_dist[next_round - 1] - car.real_dist) / car.cur_speed
        if time_to_turn < 0:
            return False
        dist = (self.track.real_dist[next_round] - my_car.real_dist)
        can_drive_dist = (self.mass * factor / (self.gamma ** 2) - my_car.cur_speed * self.mass * self.inv_gamma) * \
                         math.exp(-self.gamma * time_to_turn / self.mass) + 1.0 * self.inv_gamma * time_to_turn * factor
        logger.warning("dist= "+str(dist)+" can_drive="+str(can_drive_dist)+" offset="+str(self.mass * self.inv_gamma ** 2 / 20))
        offset = self.throttle_factor*self.mass * self.inv_gamma ** 2 / 10
        if dist < can_drive_dist - offset:
            logger.warning("True")
            return True
        return False
