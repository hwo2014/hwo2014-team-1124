__author__ = 'dima'

from bot_analyzer import ABot
from bot import logger
import math


class FixedSpeedBot(ABot):
    def __init__(self, socket, name, key):
        super(FixedSpeedBot, self).__init__(socket, name, key)
        self.friction_force = 2.15

    def on_crash(self, data, **kwargs):
        car = self.cars.cars[self.name]
        #print "crash " + str(self.max_speed[car.cur_pos.pieceIndex]) + " speed " + str(car.cur_speed)
        logger.info("crash, m v^2 / R = %f, gamma=%f, mass=%f, v=%f" %
                    (self.mass * self.get_my_car().cur_speed ** 2 / 110,
                    self.gamma,
                    self.mass,
                    self.get_my_car().cur_speed))
        exit()

    def calculate_throttle(self):
        car = self.cars.cars[self.name]

        if self.tick < 6:
            return 1.0

        if self.tick == 6:
            self.calculate_params()
            logger.info("gamma={0} ".format(self.gamma))
            logger.info("m={0} ".format(self.mass))
        if self.tick >= 6:
            if self.track.pieces[self.get_my_car().cur_pos.pieceIndex].is_round():
                return 0.70
            force = 2.56
            speed = math.sqrt(force / self.mass * 110.0)
            logger.info("m v^2 / R = {0}".format(self.mass * self.get_my_car().cur_speed ** 2 / 110))
            logger.info("sp {0}".format(speed))
            if self.get_my_car().cur_speed < speed:
                return 1.0
            else:
                return self.gamma * speed
