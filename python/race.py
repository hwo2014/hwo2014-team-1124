import socket
import random


def race(argv, action, clazz):
    host, port, name, key = argv[1:5]
    print("Connecting with parameters:")
    print("host={0}, port={1}, bot name={2}, key={3}".format(*argv[1:5]))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))
    ai_bot = clazz(s, name, key)
    if action == "join":
        ai_bot.join()
    if action == "createRace":
        ai_bot.create_race()
    if action == "joinRace":
        ai_bot.join_race()
    if action == "findRace":
        ai_bot.find_race()
    ai_bot.run()