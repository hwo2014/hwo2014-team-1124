import sys
import bot
import race
import super_bot
import fbot
import bbot
import fixedspeed
import superoverbot

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey [botclass]")
    else:
        if len(sys.argv) > 5:
            bots = {"bot" : bot.Bot,
                    "super_bot": super_bot.SBot,
                    "fbot": fbot.FBot,
                    "bbot": bbot.BBot,
                    "fixedspeed": fixedspeed.FixedSpeedBot,
                    "superoverbot": superoverbot.SuperOverBot}
            race.race(sys.argv[:5], "joinRace", bots[sys.argv[5]])
        else:
            race.race(sys.argv, "joinRace", superoverbot.SuperOverBot)