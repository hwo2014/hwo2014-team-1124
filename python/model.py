import math


class Position:
    def __init__(self, data):
        self.name = data["id"]["name"]

        self.angle = data["angle"]
        self.pieceIndex = data["piecePosition"]["pieceIndex"]
        self.inPieceDistance = data["piecePosition"]["inPieceDistance"]
        self.startLane = data["piecePosition"]["lane"]["startLaneIndex"]
        self.endLane = data["piecePosition"]["lane"]["endLaneIndex"]
        self.lap = data["piecePosition"]["lap"]


class Piece:
    def __init__(self, data):
        if "length" in data:
            self.type = "straight"
            self.length = data["length"]
        else:
            self.type = "round"
            self.radius = data["radius"]
            self.angle = data["angle"]

        self.switch = ("switch" in data) and (data["switch"])

    def is_straight(self):
        return self.type == "straight"

    def is_round(self):
        return self.type == "round"


class Track:
    def __init__(self, data):
        self.id = data["id"]
        self.name = data["name"]
        self.pieces = map(Piece, data["pieces"])
        self.n_pieces = len(self.pieces)
        self.lanes = dict(map(lambda x: (x["index"], Lane(x)), data["lanes"]))  # {index: lane}
        self.n_lanes = len(self.lanes)

        self.real_dist = [self.calc_real_dist(i) for i in xrange(self.n_pieces)]

    def calc_real_dist(self, ind):
        dist = 0
        for i in xrange(ind + 1):
            if self.is_straight(i):
                dist += self.pieces[i].length
            else:
                piece = self.pieces[i]
                lane_dist = self.lanes[0].distanceFromCenter
                radius = piece.radius
                angle = piece.angle
                if piece.angle < 0:
                    radius += lane_dist
                    angle = -angle
                else:
                    radius -= lane_dist
                dist += radius * math.pi * angle / 180

        return dist

    def is_straight(self, ind):
        return self.pieces[ind].type == "straight"

    def is_round(self, ind):
        return self.pieces[ind].type == "round"

    def next_round_piece(self, ind):
        i = ind
        while self.pieces[i].type == "straight":
            i += 1
            i = i % self.n_pieces
        return i


class Lane:
    def __init__(self, data):
        self.index = data["index"]
        self.distanceFromCenter = data["distanceFromCenter"]


class Car:
    def __init__(self, data):
        self.name = data["id"]["name"]

        self.width = data["dimensions"]["width"]
        self.length = data["dimensions"]["length"]
        self.guideFlagPosition = data["dimensions"]["guideFlagPosition"]
        self.width = data["dimensions"]["width"]

        self.cur_pos = None
        self.hist_pos = []
        self.cur_speed = 0
        self.hist_speed = []

        self.real_dist = 0
        self.hist_real_dist = []

        self.crashed = False

    def append_position(self, position, track):
        self.cur_pos = position
        self.hist_pos.append(position)

        self.cur_speed = self.calc_speed(track)
        self.hist_speed.append(self.cur_speed)

        if position.pieceIndex == 0:
            self.real_dist = self.real_in_piece(track, 0)
        else:
            self.real_dist = self.real_in_piece(track, 0) + track.real_dist[position.pieceIndex - 1]
        self.hist_real_dist.append(self.real_dist)

    def real_in_piece(self, track, lane=None):
        if len(self.hist_pos) < 2:
            return self.cur_pos.inPieceDistance

        cur_pos = self.cur_pos
        piece = track.pieces[self.cur_pos.pieceIndex]

        if piece.is_straight():
            dist = cur_pos.inPieceDistance
        else:
            n_lane = cur_pos.startLane
            if lane:
                n_lane = lane

            lane_dist = track.lanes[n_lane].distanceFromCenter
            radius = piece.radius
            angle = piece.angle
            if piece.angle < 0:
                radius += lane_dist
                angle = -angle
            else:
                radius -= lane_dist
            dist = radius * math.pi * angle / 180
        return dist

    def calc_speed(self, track):
        if len(self.hist_pos) < 2:
            return 0

        prev_pos, cur_pos = self.hist_pos[-2:]
        if cur_pos.pieceIndex == prev_pos.pieceIndex:
            dist = cur_pos.inPieceDistance - prev_pos.inPieceDistance
        else:
            prev_piece = track.pieces[prev_pos.pieceIndex]
            if prev_piece.is_straight():
                dist1 = prev_piece.length - prev_pos.inPieceDistance
            else:
                prev_lane = prev_pos.startLane
                lane_dist = track.lanes[prev_lane].distanceFromCenter
                radius = prev_piece.radius
                angle = prev_piece.angle
                if prev_piece.angle < 0:
                    radius += lane_dist
                    angle = -angle
                else:
                    radius -= lane_dist
                dist1 = radius * math.pi * angle / 180 - prev_pos.inPieceDistance
            dist = dist1 + cur_pos.inPieceDistance
        return dist / 1  # distance per one tick


class Cars:
    def __init__(self, data):
        self.cars = dict(map(lambda x: (x["id"]["name"],
                                        Car(x)), data))

    def __getitem__(self, name):
        return self.cars[name]

    def __setitem__(self, name, value):
        self.cars[name] = value

    def append_positions(self, data, track):
        positions = dict(map(lambda x: (x["id"]["name"],
                                        Position(x)), data))
        for name, car in self.cars.iteritems():
            car.append_position(positions[name], track)


