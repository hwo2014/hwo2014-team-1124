import json
from model import Track, Cars
import logging
import time

FILE_NAME = "track.cfg"

log_name = "logs/" + str(time.time()) + ".log"
logger = logging.getLogger('bot')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(log_name)
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
# create formatter and add it to the handlers
formatter = logging.Formatter(u'%(levelname)-8s [%(asctime)s] %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
#logger.addHandler(fh)
#logger.addHandler(ch)


class Bot(object):
    def __init__(self, socket, name, key):
        self.is_qualification = False
        self.durationMs = 0
        self.ms_per_tick = 16
        self.socket = socket
        self.name = name
        self.key = key
        self.track = None
        self.cars = None
        self.laps = 0
        self.maxLapTimeMs = 0
        self.quickRace = False
        self.desired_lane = 0
        self.is_turbo_available = False
        self.throttle_factor = 1
        self.tick = 0
        self.game_type = None
        self.crashed = False


    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": self.tick}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        self.msg("join", {"name": self.name,
                          "key": self.key})

    def get_track_info(self):
        with open(FILE_NAME) as f:
            lines = f.read().splitlines()
        track_name = lines[0]
        pw = lines[1]
        car_count = int(lines[2])
        return track_name, pw, car_count

    def get_my_car(self):
        return self.cars[self.name]

    def create_race(self):
        track_name, pw, car_count = self.get_track_info()
        msg_type = "createRace"
        data = {
            "botId": {
                "name": self.name,
                "key": self.key
            },
            "trackName": track_name,
            "password": pw,
            "carCount": car_count
        }
        self.msg(msg_type, data)

    def join_race(self):
        track_name, pw, car_count = self.get_track_info()
        msg_type = "joinRace"
        data = {
            "botId": {
                "name": self.name,
                "key": self.key
            },
            "trackName": track_name,
            "password": pw,
            "carCount": car_count
        }
        self.msg(msg_type, data)

    def find_race(self):
        track_name, pw, car_count = self.get_track_info()
        msg_type = "joinRace"
        data = {
            "botId": {
                "name": self.name,
                "key": self.key
            },
            "trackName": track_name,
            "password": pw,
            "carCount": car_count
        }
        self.msg(msg_type, data)

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switch_lane(self, lane):
        self.msg("switchLane", lane)

    def ping(self):
        self.msg("ping", {})

    def turbo(self, message="Trrrr-trr-trr"):
        self.msg("turbo", message)
        self.is_turbo_available = False
        logger.info("Turbo " + str(self.cars.cars[self.name].cur_pos.pieceIndex))

    def on_join(self, msgType, data, **kwargs):
        logger.info("Joined")
        self.ping()

    def on_game_start(self, data, **kwargs):
        self.crashed = False
        logger.info("Race started")
        self.throttle(1)

    def on_game_end(self, data, **kwargs):
        logger.info("Race ended")
        logger.info(data["results"])
        self.ping()
        self.tick = 0

    def on_error(self, data, **kwargs):
        logger.info("Error: {0}".format(data))
        self.ping()

    def inc_desired(self):
        self.desired_lane = min(self.track.n_lanes - 1, self.desired_lane + 1)

    def dec_desired(self):
        self.desired_lane = max(self.desired_lane - 1, 0)

    def on_car_positions(self, msgType, data, gameId, **kwargs):
        logger.debug("Car position received")
        self.cars.append_positions(data, self.track)
        #logger.info("stats: %f,%f,%d,%f" % (self.get_my_car().cur_pos.inPieceDistance,
        #                                    self.get_my_car().cur_speed,
        #                                    self.get_my_car().cur_pos.pieceIndex,
        #                                    self.get_my_car().real_dist))
        if "gameTick" in kwargs:
            self.tick = kwargs["gameTick"]
        else:
            self.desired_lane = self.get_my_car().cur_pos.endLane
            self.ping()
            return
        if self.crashed:
            self.throttle(0.3)
            #self.ping()
            return
            #logger.info("Car position received")
        #logger.info("stats: %f,%f,%d,%f" % (self.get_my_car().cur_pos.inPieceDistance,
        #                                    self.get_my_car().cur_speed,
        #                                    self.get_my_car().cur_pos.pieceIndex,
        #                                    self.get_my_car().real_dist))
        new_lane = self.need_change_lane()
        my_lane = self.get_my_car().cur_pos.endLane
        #logger.info("line des %d" % self.desired_lane)
        #logger.info("line my %d" % my_lane)
        if self.desired_lane == my_lane:

            if new_lane > my_lane:
                logger.info("line Switch to right")
                self.switch_lane("Right")
                self.inc_desired()
                return
            elif new_lane < my_lane:
                logger.info("line Switch to left")
                self.switch_lane("Left")
                self.dec_desired()
                return
        if self.is_turbo_available and self.need_turbo():
            self.turbo()
            return
        try:
            throttle = self.calculate_throttle()
        except Exception as e:
            logger.error("Exception setting throttle to 0.5")
            logger.error(e)
            throttle = 0.5

        if throttle > 1:
            throttle = 1
        if throttle < 0:
            throttle = 0
        self.throttle(throttle)

    def calculate_throttle(self):
        return 0.3

    def need_turbo(self):
        return False

    def need_change_lane(self):
        # returns index of lane
        return self.get_my_car().cur_pos.endLane

    def on_crash(self, data, **kwargs):
        logger.info(data["name"] + " crashed :((")
        if data["name"] == self.name:
            #self.desired_lane = self.get_my_car().cur_pos.startLane
            logger.info("turbo available="+str(self.is_turbo_available))
            self.crashed = True
        self.cars[data["name"]].crashed = True
        self.ping()

    def on_your_car(self, msgType, data, **argv):
        self.name = data["name"]
        logger.info("my car " + self.name)
        self.ping()

    def on_game_init(self, msgType, data, **argv):
        self.tick = 0
        race = data["race"]
        self.crashed = False
        self.track = Track(race["track"])
        self.cars = Cars(race["cars"])

        logger.debug(race)
        try:
            self.laps = race["raceSession"]["laps"]
            self.maxLapTimeMs = race["raceSession"]["maxLapTimeMs"]
            self.is_qualification = False
            self.quickRace = race["raceSession"]["quickRace"]
        except:
            try:
                self.durationMs = race["raceSession"]["durationMs"]
                self.is_qualification = True
                self.laps = 6
                logger.info("qualification")
            except:
                # ignore exception
                logger.error("Weird race but still trying to do something")
                self.laps = 6

        logger.info("Game init")
        self.ping()

    def on_turbo_available(self, data, **kwargs):
        self.is_turbo_available = True
        self.turboDurationMilliseconds = data["turboDurationMilliseconds"]
        self.turboDurationTicks = data["turboDurationTicks"]
        self.turboFactor = data["turboFactor"]
        self.ping()

    def on_turbo_start(self, data, **kwargs):
        logger.info("Turbo start " + str(self.cars.cars[self.name].cur_pos.pieceIndex))
        self.throttle_factor = self.turboFactor
        self.ping()

    def on_turbo_end(self, **kwargs):
        logger.info("Turbo end")
        self.throttle_factor = 1
        self.ping()

    def on_spawn(self, data, **kwargs):
        logger.warning("spawn")
        if data["name"] == self.name:
            self.crashed = False
            self.is_turbo_available = False
        self.cars[data["name"]].crashed = False
        self.ping()

    def run(self):
        self.msg_loop()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'yourCar': self.on_your_car,
            'turboAvailable': self.on_turbo_available,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'spawn': self.on_spawn
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](**msg)
            else:
                logger.info("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
