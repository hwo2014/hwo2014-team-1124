from bot_analyzer import *
import random

MAX_SPEED = 10000


class SBot(ABot):
    def __init__(self, socket, name, key):
        super(SBot, self).__init__(socket, name, key)
        self.low_friction_force = 1.5
        self.high_friction_force = 4.2

        self.friction_force = (self.low_friction_force + self.high_friction_force) / 2.0
        self.update_friction_force_ticks = 60
        self.current_friction_force_tick = 0
        self.critical_angle = 60
        self.piece_length = []
        self.max_speed = []
        self.absIndex = 0
        self.difficulty_list = []
        self.switch_indexes = set([])
        #high for race
        #low for half of the qualification to learn friction
        self.speed_mode = "low"
        #additional for friction force determination
        self.max_danger = 0
        self.max_difficulty = 0
        self.max_percent = 0
        #for turbo
        self.straight_max = (0, 0)

    def calc_radius(self, endLane, piece):
        lane_dist = self.track.lanes[endLane].distanceFromCenter
        radius = piece.radius
        angle = piece.angle
        if piece.angle < 0:
            radius += lane_dist
            angle = -angle
        else:
            radius -= lane_dist
        return angle, radius

    def init_optimize(self):
        m = len(self.track.pieces)
        n = m * self.laps
        lane_number = len(self.track.lanes)
        for i in range(n):
            index = i % m
            piece = self.track.pieces[index]
            lane_list_length = []
            lane_list_max_speed = []
            for j in range(lane_number):
                if piece.is_round():
                    angle, radius = self.calc_radius(j, piece)
                    plength = radius * math.pi * angle / 180
                    lane_list_length.append(plength)
                    max_sp = self.force_to_speed(self.friction_force, j, piece)
                    lane_list_max_speed.append(max_sp)
                else:
                    lane_list_length.append(piece.length)
                    lane_list_max_speed.append(MAX_SPEED)
            self.piece_length.append(lane_list_length)
            self.max_speed.append(lane_list_max_speed)
        mass = self.mass
        gamma = self.gamma
        factor = (gamma ** 2) / mass
        for i in range(n - 2, 0, -1):
            for j in range(lane_number):
                v = self.max_speed[i][j]
                f3 = v * self.gamma
                v = self.max_speed[i - 1][j]
                f1 = v * self.gamma
                if f1 > f3:
                    relaxation_l = self.calc_relaxation_length(f1, f3)
                    if relaxation_l > self.piece_length[i - 1][j]:
                        self.max_speed[i - 1][j] = self.inv_gamma * (factor * self.piece_length[i - 1][j] + f3)
        self.difficulty_list = [[0 for _ in range(lane_number)] for _ in range(m)]
        max_difficulty = 0
        for j in range(lane_number):
            for i in range(m):
                l1 = 0
                l2 = 0
                for ii in range(m):
                    difficulty = 0
                    piece = self.track.pieces[(i + ii) % m]
                    if piece.is_round():
                        angle, radius = self.calc_radius(j, piece)
                        difficulty += 1.0 / radius * math.exp(-0.5 * l1 / (self.mass / (self.gamma ** 2)))
                        l1 += self.piece_length[(i + ii) % m][j]
                    piece = self.track.pieces[(i - ii) % m]
                    if piece.is_round():
                        angle, radius = self.calc_radius(j, piece)
                        difficulty += 1.0 / radius * math.exp(-0.5 * l2 / (self.mass / (self.gamma ** 2)))
                        l2 += self.piece_length[(i - ii) % m][j]
                    self.difficulty_list[i][j] += difficulty
                if max_difficulty < self.difficulty_list[i][j]:
                    max_difficulty = self.difficulty_list[i][j]
        for j in range(lane_number):
            for i in range(m):
                self.difficulty_list[i][j] /= max_difficulty

    def on_crash(self, data, **kwargs):
        if data["name"] == self.name:
            try:
                car = self.cars.cars[self.name]
                self.critical_angle = max(max(0.1, car.cur_pos.angle), -car.cur_pos.angle)
                #logger.warn(" ind=" + str(self.absIndex) + " max speed=" + str(
                #    self.max_speed[self.absIndex][car.cur_pos.endLane]) + " speed=" +
                #            str(car.hist_speed[-1]) + " speed=" + str(car.hist_speed[-2]))
                self.current_friction_force_tick = 0
                self.dec_friction_force(self.max_danger, self.max_difficulty, self.max_percent)
                self.max_danger = 0
                self.max_difficulty = 0
                self.max_percent = 0
            except:
                logger.warn("exception happened in on_crash superbot")
        super(SBot, self).on_crash(data, **kwargs)


    #f3 between 0 and f1
    def calc_relaxation_length(self, f1, f3):
        m = self.mass
        gamma = self.gamma
        factor = m / (gamma ** 2)
        return factor * (f1 - f3 )

    def on_game_init(self, msgType, data, **argv):
        super(SBot, self).on_game_init(msgType, data, **argv)
        self.piece_length = []
        self.max_speed = []
        self.absIndex = 0
        self.switch_indexes = set([])
        if self.track.name.lower() == "suzuka" and self.is_qualification:
            self.low_friction_force = 1.5
            self.high_friction_force = 3.0
            self.friction_force = (self.low_friction_force + self.high_friction_force) / 2.0


    def on_game_end(self, data, **argv):
        super(SBot, self).on_game_end(data, **argv)
        self.friction_force *= 0.94
        self.low_friction_force *= 0.94
        self.high_friction_force *= 0.94

        means = {}
        for name, car in self.cars.cars.iteritems():
            means[name] = sum(car.hist_speed) / len(car.hist_speed)


    def dec_friction_force(self, danger, difficulty, percent):
        if percent > 2:
            self.high_friction_force = self.high_friction_force * 0.9 + self.friction_force * 0.1
            self.friction_force = (self.high_friction_force + self.low_friction_force) / 2.0
        else:
            self.high_friction_force = self.friction_force
            self.low_friction_force = 0.9 * self.low_friction_force
            self.friction_force = (self.high_friction_force + self.low_friction_force) / 2.0
        self.init_optimize()
        logger.warning("decreased friction force to " + str(self.friction_force))
        logger.warning(self.speed_mode)


    def inc_friction_force(self, danger, difficulty, percent):
        l = self.low_friction_force
        h = self.high_friction_force
        a = self.friction_force
        if percent > 1:
            l = l * 0.7 * danger + a * (1 - 0.7 * danger)
        else:
            logger.warning("l=" + str(l) + " r=" + str(h))
            logger.warning("dif=" + str(difficulty) + " dan=" + str(danger) + " percent=" + str(percent))
            l = l * (1 - difficulty) + difficulty * (
                danger * l + (1 - danger) * (l * (1 - 0.5 * percent) + a * 0.5 * percent))
            #h = h * (1 - difficulty) + difficulty * ((1 - danger) * h + danger * (h * (1 - percent) + a * percent))
            h = a * 0.9 * danger + (1 - 0.9 * danger) * (h * (1 - 0.5 * percent) + a * 0.5 * percent)
            logger.warning("l=" + str(l) + " r=" + str(h))
        self.high_friction_force = h
        self.low_friction_force = l
        self.friction_force = max((self.high_friction_force + self.low_friction_force) / 2.0, self.friction_force)
        self.init_optimize()
        logger.warning("increased friction force to " + str(self.friction_force))
        logger.warning(self.speed_mode)


    def calculate_throttle(self):
        if not self.is_qualification:
            self.speed_mode = "low"
        elif self.durationMs * 0.6 - self.tick * self.ms_per_tick > 0:
            self.speed_mode = "high"
            self.update_friction_force_ticks = max(min(self.durationMs * 0.05 * self.ms_per_tick, 50), 200)
        else:
            self.speed_mode = "low"
        car = self.cars.cars[self.name]
        if self.tick == 0:
            cur_pos = car.cur_pos
            self.absIndex = cur_pos.pieceIndex

        if len(car.hist_pos) > 1:
            prev_pos, cur_pos = car.hist_pos[-2:]
            if cur_pos.pieceIndex != prev_pos.pieceIndex:
                self.absIndex += 1
                self.absIndex = self.absIndex % len(self.max_speed)
            index = cur_pos.pieceIndex
            if index != self.absIndex % len(self.track.pieces):
                self.absIndex = index + len(self.track.pieces) * (self.absIndex / len(self.track.pieces))

        if self.tick < 6:
            return 1.0

        if self.tick == 6:
            self.calculate_params()
            logger.debug("gamma={0} ".format(self.gamma))
            logger.debug("m={0} ".format(self.mass))
            self.init_optimize()


        #lane
        lane_index = car.cur_pos.endLane
        total_length = self.piece_length[self.absIndex][lane_index]
        piece = self.track.pieces[car.cur_pos.pieceIndex]

        x = car.cur_pos.inPieceDistance
        lx = total_length - x
        next_max_speed = MAX_SPEED
        if self.absIndex < len(self.max_speed) - 1:
            next_max_speed = self.max_speed[self.absIndex + 1][lane_index]

        f1 = car.cur_speed * self.gamma
        f3 = next_max_speed * self.gamma
        rl = self.calc_relaxation_length(f1, f3)

        if self.absIndex < len(self.piece_length):
            logger.debug("max speed " + str(self.max_speed[self.absIndex][car.cur_pos.endLane]) + " speed " + str(
                car.cur_speed) + " next max speed " + str(next_max_speed) + " left " + str(lx) + " rl " + str(rl))
        is_stop = False
        throttle = 0.01
        if next_max_speed < car.cur_speed:
            if rl >= lx:
                logger.debug(" stop " + str(0.0))
                is_stop = True
                throttle = 0.0
        if not is_stop:
            if piece.is_round():
                throttle = self.friction_force - self.speed_to_force(car.cur_speed, car.cur_pos.endLane, piece)
                throttle = min(1.0, throttle)
                throttle = max(0.0, throttle)
                logger.debug(" round " + str(throttle))
                throttle = throttle / self.throttle_factor
            else:
                throttle = 1.0
                next_f1 = self.gamma * (
                    (1 - self.gamma / self.mass) * car.cur_speed + throttle * self.throttle_factor / self.mass)
                next_left = lx - car.cur_speed
                if next_left < self.calc_relaxation_length(next_f1, f3):
                    throttle = 0.0
                logger.debug(" straight " + str(throttle))

        #calculate friction params
        ang = car.cur_pos.angle
        if (ang < 0):
            ang = -ang
        self.max_danger = min(1, ang / self.critical_angle)
        self.max_difficulty = max(self.max_difficulty, self.difficulty_list[car.cur_pos.pieceIndex][lane_index])
        if piece.is_round():
            max_force = self.friction_force
            cur_force = throttle + self.speed_to_force(car.cur_speed, car.cur_pos.endLane, piece)
            self.max_percent = max(self.max_percent,
                                   cur_force / max_force)
        else:
            self.max_percent = max(self.max_percent, 0)
        if logger.isEnabledFor(logging.DEBUG):
            logger.debug(
                " danger=" + str(self.max_danger) + " difficulty=" + str(self.max_difficulty) + " percent=" + str(
                    self.max_percent))
        self.current_friction_force_tick += 1
        #end calc friction params

        if self.speed_mode == "high": #or self.speed_mode == "low":
            if self.current_friction_force_tick == self.update_friction_force_ticks:
                self.current_friction_force_tick = 0
                self.inc_friction_force(self.max_danger, self.max_difficulty, self.max_percent)
                self.max_danger = 0
                self.max_difficulty = 0
                self.max_percent = 0

        return throttle

    def speed_to_force(self, speed, lane, piece):
        m = self.mass
        angle, radius = self.calc_radius(lane, piece)
        alpha = 1.0
        return m * (speed ** 2) / (alpha * radius)

    def force_to_speed(self, force, lane, piece):
        m = self.mass
        angle, radius = self.calc_radius(lane, piece)
        alpha = 1.0
        return math.sqrt(force * (radius * alpha) / m)

    def speed_to_throttle(self, speed):
        return min(1.0, speed * self.gamma)

    def need_change_lane(self):
        if len(self.piece_length) == 0:
            return self.desired_lane
        car = self.cars.cars[self.name]
        abs_ind = self.absIndex

        my_lane = car.cur_pos.endLane
        if self.desired_lane != my_lane:
            return self.desired_lane
        ind = (abs_ind + 1) % len(self.track.pieces)
        next_piece = self.track.pieces[ind]
        if abs_ind in self.switch_indexes:
            return self.desired_lane
        if next_piece.switch:
            self.switch_indexes.add(abs_ind)
            left_lane = max(my_lane - 1, 0)
            right_lane = min(my_lane + 1, len(self.track.lanes) - 1)
            left_dist = 0
            my_dist = 0
            right_dist = 0
            if next_piece.is_round():
                left_dist += (self.piece_length[ind][left_lane] + self.piece_length[ind][my_lane]) / 2.0
                my_dist += self.piece_length[ind][my_lane]
                right_dist += (self.piece_length[ind][right_lane] + self.piece_length[ind][my_lane]) / 2.0
            while True:
                ind = (ind + 1) % len(self.track.pieces)
                left_dist += self.piece_length[ind][left_lane]
                my_dist += self.piece_length[ind][my_lane]
                right_dist += self.piece_length[ind][right_lane]
                if self.track.pieces[ind].switch:
                    break
            if left_dist < my_dist and left_dist < right_dist:
                logger.info(str(ind) +
                            " switch_left " + str(left_lane) + " " + str(self.desired_lane) + " " + str(
                    car.cur_pos.endLane))
                return left_lane
            if right_dist < my_dist and right_dist < left_dist:
                logger.info(str(ind) +
                            " switch_right " + str(right_lane) + " " + str(self.desired_lane) + " " + str(
                    car.cur_pos.endLane))
                return right_lane
            logger.info(
                str(ind) + " stay " + str(my_lane) + " " + str(self.desired_lane) + " " + str(car.cur_pos.endLane))
            return my_lane
        else:
            return self.desired_lane

